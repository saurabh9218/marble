package com.example.ektagomitbharani.marble;

import android.os.AsyncTask;
import android.util.Log;

import de.greenrobot.event.EventBus;

/**
 * Created by Sau on 6/26/15.
 */
public class AccountController {


    public static class StartProgress {
        ;
    }

    public static class StopProgress {
        ;
    }

    public static class Error {
        ;
    }

    public static void updatUser(User user) {

        EventBus.getDefault().post(new AccountController.StartProgress());
        new UserUpdateAsync().execute(user);
    }

    protected static class UserUpdateAsync extends AsyncTask<User, Void, Void> {

        @Override
        protected Void doInBackground(User... users) {

            User user = null;
            long updateSuccess = -1;
            if (users.length == 1) {
                user = users[0];
                updateSuccess = Database.UpdateUserWithId(MyApplication.getUser().getUSER_NAME(), user);
            } else
                Log.i("Ekta Bharani", "Bad number of arguments: required 1 found " + users.length);

            EventBus.getDefault().post(new AccountController.StopProgress());
            if (updateSuccess != -1)
                EventBus.getDefault().post(user);

            else
                EventBus.getDefault().post(new AccountController.Error());

            return null;
        }
    }
}
