package com.example.ektagomitbharani.marble;

/**
 * Created by Sau on 7/15/15.
 */

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;


public class ServiceForSMS extends IntentService {

    public ServiceForSMS() {
        super("ServiceForSMS");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            if (!intent.getExtras().isEmpty()) {
                Bundle b = intent.getExtras();
                String name = b.get(Constants.MEDICATION_NAME)
                        + "";
                String message = "Dosage Reminder\n You have to take " + name + " now!";
                SmsManager smsManager = SmsManager.getDefault();

                String number = MyApplication.getUser().getPHONE();
                smsManager.sendTextMessage(number, null, message, null, null);
                Log.i("MYLIST", "Message " + message + " sent to " + number);
            }
        }
    }
}
