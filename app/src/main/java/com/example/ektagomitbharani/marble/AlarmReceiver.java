package com.example.ektagomitbharani.marble;

/**
 * Created by Sau on 7/13/15.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent arg1) {
        Intent service1 = new Intent(context, MyAlarmService.class);
        service1.putExtra(Constants.MEDICATION_NAME, arg1.getStringExtra(Constants.MEDICATION_NAME));
        service1.putExtra(Constants.START_DATE, arg1.getStringExtra(Constants.START_DATE));
        service1.putExtra(Constants.NOTIFICATION_ALARM, arg1.getIntExtra(Constants.NOTIFICATION_ALARM, 0));
        service1.putExtra(Constants.NOTIFICATION_EMAIL, arg1.getIntExtra(Constants.NOTIFICATION_EMAIL, 0));
        context.startService(service1);

    }

}

