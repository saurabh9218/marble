package com.example.ektagomitbharani.marble;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;


public class ScheduleAdapter extends ResourceCursorAdapter {
    public ScheduleAdapter(Context context, int layout, Cursor c, int flags) {

        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView qty = (TextView) view.findViewById(R.id.qty);
        TextView time = (TextView) view.findViewById(R.id.time);
        TextView pills = (TextView) view.findViewById(R.id.pills);
        TextView date = (TextView) view.findViewById(R.id.date);

        date.setText("" + cursor.getString(cursor.getColumnIndex(Constants.START_DATE)));

        qty.setText("" + cursor.getString(cursor.getColumnIndex(Constants.MG)) + " mg");
        time.setText("" + cursor.getString(cursor.getColumnIndex(Constants.START_TIME)));
        name.setText("" + cursor.getString(cursor.getColumnIndex(Constants.MEDICATION_NAME)));
        pills.setText("" + cursor.getString(cursor.getColumnIndex(Constants.PILL_QTY)) + " pill(s)");

    }
}

