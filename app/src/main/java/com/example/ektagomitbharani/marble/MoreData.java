package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import de.greenrobot.event.EventBus;


public class MoreData extends Activity {

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_data);
        EventBus.getDefault().register(this);
        findViewById(R.id.more_data_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processData();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void processData() {

        CheckBox alarm = (CheckBox) findViewById(R.id.alarm);
        CheckBox text = (CheckBox) findViewById(R.id.text);
        CheckBox email = (CheckBox) findViewById(R.id.email);

        if (!alarm.isChecked() && !text.isChecked() && !email.isChecked()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(MoreData.this);
            dialog.setMessage("Please select any one option!")
                    .setTitle("Alert!")
                    .setPositiveButton(android.R.string.ok, null);
            AlertDialog dialogBox = dialog.create();
            dialogBox.show();
        } else {

            Intent intent = getIntent();
            String medicationName = intent.getStringExtra(Constants.MEDICATION_NAME);
            String username = intent.getStringExtra(Constants.USERNAME);
            Log.i("Ekta Bharani", "More Data " + username);
            int mgQty = intent.getIntExtra(Constants.MG, 0);
            int isSchedule = intent.getIntExtra(Constants.SCHEDULE, 1);

            int pillQty = intent.getIntExtra(Constants.PILL_QTY, 0);
            String startDate = intent.getStringExtra(Constants.START_DATE);
            String endDate = intent.getStringExtra(Constants.END_DATE);
            int neverEnding = intent.getIntExtra(Constants.NEVER_ENDING, 0);
            String startTime = intent.getStringExtra(Constants.START_TIME);
            int frequency = intent.getIntExtra(Constants.FREQUENCY, 0);

            int a = alarm.isChecked() ? 1 : 0;
            int t = text.isChecked() ? 1 : 0;
            int e = email.isChecked() ? 1 : 0;

            final MedicationData data = new MedicationData(
                    medicationName,
                    username,
                    mgQty,
                    isSchedule,
                    pillQty,
                    startDate,
                    startTime,
                    endDate,
                    neverEnding,
                    frequency,
                    a, t, e
            );

            MyApplication.getMd().setNotificationAlarm(a);
            MyApplication.getMd().setNotificationEmail(e);
            MyApplication.getMd().setNotificationText(t);
            Intent newIntent = new Intent(MoreData.this, DosageTime.class);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(newIntent);


            /*
            AlertDialog.Builder dialog = new AlertDialog.Builder(MoreData.this);

            dialog.setMessage("Do you want self scheduling?")
                    .setTitle("Alert!")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(MoreData.this, DosageTime.class));
                            MyApplication.p("self");
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MyApplication.p("auto");
                            MyApplication.getMd().addMedication();
                        }
                    });

            AlertDialog dialogBox = dialog.create();
            dialogBox.show();
            */
        }
    }

    public void onEventMainThread(MedicationData.MedicationToHomeActivity medicationToHomeActivity) {
        startActivity(new Intent(MoreData.this, Home.class));
        finish();
    }

    public void onEventMainThread(MedicationData.ReturnToMedicationActivity returnToMedicationActivity) {
        new AlertDialog.Builder(MoreData.this)
                .setTitle(R.string.alert)
                .setMessage(R.string.duplicate)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(MoreData.this, Medication.class));
                        finish();
                    }
                })
                .show();
    }

    public void onEventMainThread(Constants.StartProgress start) {

        progressDialog = ProgressDialog.show(MoreData.this, getString(R.string.progress_title), getString(R.string.progress_subtitle));
    }

    public void onEventMainThread(Constants.StopProgress stop) {
        progressDialog.dismiss();
    }


}
