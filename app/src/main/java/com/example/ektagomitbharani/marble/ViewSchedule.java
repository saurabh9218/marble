package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class ViewSchedule extends Activity implements View.OnClickListener {
    String str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_schedule);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.button1:
                str = "Today";
                break;
            case R.id.button2:
                str = "Tomorrow";
                break;
            case R.id.button3:
                str = "Weekly";
                break;
            case R.id.button4:
                str = "Monthly";


        }
        schedule();
    }

    private void schedule() {
        Intent i = new Intent(ViewSchedule.this, Schedule.class);
        i.putExtra("Schedule", str);
        startActivity(i);
    }
}
