package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import de.greenrobot.event.EventBus;


public class MainActivity extends Activity {

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkUserLoggedIn();
        setContentView(R.layout.activity_main);

        EventBus.getDefault().register(this);
        // Database.addUser(new User("sau69","sau69","Saurabh","Jain","saurabh.jain@nyu.edu","9876543221"));
        Button b1 = (Button) findViewById(R.id.signin);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText e1 = (EditText) findViewById(R.id.username);
                String username = e1.getText().toString();

                EditText e2 = (EditText) findViewById(R.id.password);
                String password = e2.getText().toString();

                if (username.isEmpty() || password.isEmpty()) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle(R.string.alert)
                            .setMessage(R.string.fill_all_details)
                            .setPositiveButton(R.string.ok, null)
                            .show();

                } else
                    Controller.authenticateUser(new User(username, password));

            }
        });

        Button b2 = (Button) findViewById(R.id.account);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Account.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void checkUserLoggedIn() {
        if (MyApplication.getUser() != null)
            startHome();
    }

    private void startHome() {
        startActivity(new Intent(this, Home.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(Controller.StartProgress start) {

        progressDialog = ProgressDialog.show(MainActivity.this, getString(R.string.progress_title), getString(R.string.progress_subtitle));
    }

    public void onEventMainThread(Controller.StopProgress stop) {
        progressDialog.dismiss();
    }

    public void onEventMainThread(Controller.Error error) {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.error)
                .setMessage(R.string.no_user_found_error)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    public void onEventMainThread(User user) {
        MyApplication.setUser(user);
        startHome();
    }

}
