package com.example.ektagomitbharani.marble;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Sau on 6/19/15.
 */
public class Database extends SQLiteOpenHelper {

    private static final String TAG = "Ekta Bharani";

    private static final int VERSION = 15;
    private static final String DATABASE_NAME = "marble";

    private static final String USER_NAME = "_id";


    private static final String TABLE_NAME_MEDICATION = "medication";
    private static final String MEDICATION_NAME = "medication_name";
    private static final String MG = "mg";
    private static final String SCHEDULE = "schedule";
    private static final String NO_OF_PILLS = "no_of_pills";
    private static final String START_DATE = "start_date";
    private static final String END_DATE = "end_date";
    private static final String NEVER_ENDING = "never_ending";
    private static final String START_TIME = "start_time";
    private static final String FREQUENCY = "frequency";
    private static final String NOTIFICATION_ALARM = "alarm";
    private static final String NOTIFICATION_TEXT = "text";
    private static final String NOTIFICATION_EMAIL = "email";
    private static final String TABLE_CREATE_MEDICATION = "CREATE TABLE "
            + TABLE_NAME_MEDICATION
            + " ( "
            + USER_NAME + " TEXT, "
            + MEDICATION_NAME + " TEXT, "
            + MG + " INTEGER, "
            + SCHEDULE + " INTEGER, "
            + NO_OF_PILLS + " INTEGER, "
            + START_DATE + " TEXT, "
            + END_DATE + " TEXT, "
            + NEVER_ENDING + " INTEGER, "
            + START_TIME + " TEXT, "
            + FREQUENCY + " INTEGER, "
            + NOTIFICATION_ALARM + " INTEGER, "
            + NOTIFICATION_TEXT + " INTEGER, "
            + NOTIFICATION_EMAIL + " INTEGER, "
            + "PRIMARY KEY ( " + MEDICATION_NAME + " , " + USER_NAME + " , " + START_DATE + " , " + START_TIME + ")"
            + " );";


    private static final String TABLE_NAME = "user";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String PHONE = "phone";
    private static final String TABLE_CREATE_USER = "CREATE TABLE "
            + TABLE_NAME
            + " ( "
            + FIRST_NAME + " TEXT, "
            + LAST_NAME + " TEXT, "
            + EMAIL + " TEXT, "
            + PHONE + " INTEGER, "
            + USER_NAME + " TEXT PRIMARY KEY, "
            + PASSWORD + " TEXT "
            + " );";


    private static final String TABLE_NAME_HISTORY = "history";
    private static final String PILL_TAKEN = "pill_taken";

    private static final String TABLE_CREATE_HISTORY = " CREATE TABLE "
            + TABLE_NAME_HISTORY
            + " ( "
            + MEDICATION_NAME + " TEXT, "
            + USER_NAME + " TEXT, "
            + START_DATE + " TEXT, "
            + START_TIME + " TEXT, "
            + PILL_TAKEN + " INTEGER, "
            + "PRIMARY KEY ( " + MEDICATION_NAME + " , " + USER_NAME + " , " + START_DATE + " , " + START_TIME + " ),"
            + "FOREIGN KEY ( " + MEDICATION_NAME + " , " + START_DATE + " , " + START_TIME
            + " ) REFERENCES " + TABLE_NAME_MEDICATION + " ( " + MEDICATION_NAME + " , " + START_DATE + " , " + START_TIME + " ),"
            + "FOREIGN KEY ( " + USER_NAME + " ) REFERENCES " + TABLE_NAME + " ( " + USER_NAME + " ) "
            + " );";


    private static Database database;
    private static PendingIntent pendingIntent;


    public Database(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        //Log.i(TAG, "Constructor");
    }

    public static Database getDatabase() {

        //Log.i(TAG, TABLE_CREATE_HISTORY);
        if (database == null)
            database = new Database(MyApplication.getContext());
        return database;
    }

    public static long addMedication(MedicationData data) {

        ContentValues cv = new ContentValues();
        cv.put(MEDICATION_NAME, data.getMedicationName());
        cv.put(USER_NAME, data.getUsername());
        cv.put(MG, data.getMg());
        cv.put(SCHEDULE, data.isSchedule());
        cv.put(NO_OF_PILLS, data.getNoOfPills());
        cv.put(START_DATE, data.getStartDate());
        cv.put(START_TIME, data.getStartTime());
        cv.put(END_DATE, data.getEndTime());
        cv.put(NEVER_ENDING, data.getNeverEnding());
        cv.put(FREQUENCY, data.getFrequency());
        cv.put(NOTIFICATION_ALARM, data.isNotificationAlarm());
        cv.put(NOTIFICATION_TEXT, data.isNotificationText());
        cv.put(NOTIFICATION_EMAIL, data.isNotificationEmail());
        long id = getDatabase().getWritableDatabase().insert(TABLE_NAME_MEDICATION, null, cv);

        if (id != -1) {

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a", Locale.US);

            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(data.getStartDate() + " " + data.getStartTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            AlarmManager alarmManager = (AlarmManager) MyApplication.getContext().getSystemService(Context.ALARM_SERVICE);

            if (data.isNotificationAlarm() == 1) {

                Intent myIntent = new Intent(MyApplication.getContext(), AlarmReceiver.class);
                myIntent.putExtra(MEDICATION_NAME, data.getMedicationName());
                myIntent.putExtra(START_DATE, data.getStartDate() + " " + data.getStartTime());
                myIntent.putExtra(NOTIFICATION_ALARM, data.isNotificationAlarm());
                pendingIntent = PendingIntent.getBroadcast(MyApplication.getContext(), MyApplication.id++, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                alarmManager.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
            }

            if (data.isNotificationText() == 1) {

                Intent i = new Intent(MyApplication.getContext(), ServiceForSMS.class);
                i.putExtra(Constants.MEDICATION_NAME, data.getMedicationName());
                PendingIntent pendingIntent = PendingIntent.getService(MyApplication.getContext(), MyApplication.id++, i, PendingIntent.FLAG_UPDATE_CURRENT);
                //alarmManager.set(AlarmManager.RTC, c.getTimeInMillis(), pendingIntent);
                long time = c.getTimeInMillis();
                alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
            }
            ContentValues cv1 = new ContentValues();
            cv1.put(MEDICATION_NAME, data.getMedicationName());
            cv1.put(USER_NAME, data.getUsername());
            cv1.put(START_DATE, data.getStartDate());
            cv1.put(START_TIME, data.getStartTime());
            cv1.put(PILL_TAKEN, 0);

            getDatabase().getWritableDatabase().insert(TABLE_NAME_HISTORY, null, cv1);
        }

        //MyApplication.p("History id = " + id1 + "");
        return id;
    }

    public static void addUser(User user) {
        ContentValues cv = new ContentValues();
        cv.put(USER_NAME, user.getUSER_NAME());
        cv.put(PASSWORD, user.getPASSWORD());
        cv.put(FIRST_NAME, user.getFIRST_NAME());
        cv.put(LAST_NAME, user.getLAST_NAME());
        cv.put(EMAIL, user.getEMAIL());
        cv.put(PHONE, user.getPHONE());

        getDatabase().getWritableDatabase().insert(TABLE_NAME, null, cv);
        // Log.i(TAG, "Entry inserted");

    }

    public static int getCount() {
        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME, null, null, null, null, null, null);
        return cursor.getCount();
    }


    public static MedicationData getMedicationData(Cursor cursor) {


        MedicationData data = new MedicationData(
                cursor.getString(cursor.getColumnIndex(MEDICATION_NAME)),
                cursor.getString(cursor.getColumnIndex(USER_NAME)),
                cursor.getInt(cursor.getColumnIndex(MG)),
                cursor.getInt(cursor.getColumnIndex(SCHEDULE)),
                cursor.getInt(cursor.getColumnIndex(NO_OF_PILLS)),
                cursor.getString(cursor.getColumnIndex(START_DATE)),
                cursor.getString(cursor.getColumnIndex(START_TIME)),
                cursor.getString(cursor.getColumnIndex(END_DATE)),
                cursor.getInt(cursor.getColumnIndex(NEVER_ENDING)),
                cursor.getInt(cursor.getColumnIndex(FREQUENCY)),
                cursor.getInt(cursor.getColumnIndex(NOTIFICATION_ALARM)),
                cursor.getInt(cursor.getColumnIndex(NOTIFICATION_TEXT)),
                cursor.getInt(cursor.getColumnIndex(NOTIFICATION_EMAIL))
        );

        return data;
    }


    public static Cursor getUpcomingDosagesWithId(String id, String date) {

        String condition = USER_NAME + " = '" + id + "' and " + SCHEDULE + " = " + 1 + " and " + START_DATE + " >= '" + date + "'";

        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME_MEDICATION, null, condition, null, null, null, START_DATE + "," + START_TIME, null);

        return cursor;
    }

    public static User checkUserWithId(User user) {


        String user_name = user.getUSER_NAME();
        String password = user.getPASSWORD();
        String condition = USER_NAME + " = '" + user_name + "' and " + PASSWORD + " = '" + password + "'";
        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME, null, condition, null, null, null, null);
        if (cursor.getCount() == 0)
            return null;
        else if (cursor.getCount() != 1)
            throw new Error("Database Error On Search For Id : " + user_name);

        cursor.moveToFirst();
        String username = cursor.getString(cursor.getColumnIndex(USER_NAME));
        String pwd = cursor.getString(cursor.getColumnIndex(PASSWORD));
        String fname = cursor.getString(cursor.getColumnIndex(FIRST_NAME));
        String lname = cursor.getString(cursor.getColumnIndex(LAST_NAME));
        String email = cursor.getString(cursor.getColumnIndex(EMAIL));
        String phone = cursor.getString(cursor.getColumnIndex(PHONE));

        return new User(username, pwd, fname, lname, email, phone);
    }

    public static long UpdateUserWithId(String user_name, User user) {

        ContentValues cv = new ContentValues();
        cv.put(USER_NAME, user.getUSER_NAME());
        cv.put(PASSWORD, user.getPASSWORD());
        cv.put(FIRST_NAME, user.getFIRST_NAME());
        cv.put(LAST_NAME, user.getLAST_NAME());
        cv.put(PHONE, user.getPHONE());
        cv.put(EMAIL, user.getEMAIL());

        //Log.i(TAG, USER_NAME + " = '" + user_name + "'");
        return getDatabase().getWritableDatabase().update(TABLE_NAME, cv, USER_NAME + " = '" + user_name + "'", null);

    }

    public static Cursor getDosageHistoryDateWithIdAndCurrentDate(String user_name, String date) {

        Log.i(TAG, "Date = " + date);
        String condition = USER_NAME + " = '" + user_name + "' and " + START_DATE + " <= '" + date + "'";
        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME_HISTORY, null, condition, null, null, null, null);
        return cursor;
    }

    public static long updateHistory(History history) {

        ContentValues cv = new ContentValues();
        cv.put(PILL_TAKEN, history.getPillTaken());

        String condition = USER_NAME + " = '" + history.getUsername() + "' and "
                + START_DATE + " = '" + history.getStartDate() + "' and "
                + START_TIME + " = '" + history.getStartTime() + "' and "
                + MEDICATION_NAME + " = '" + history.getMedicationName() + "'";
        //Log.i(TAG, PILL_TAKEN + " = " + history.getPillTaken() + " " + condition);
        return getDatabase().getWritableDatabase().update(TABLE_NAME_HISTORY, cv, condition, null);

    }

    public static long updateHistorySelfMedicateWithId(History history) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        String currentDate = sdf.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(PILL_TAKEN, history.getPillTaken());
        cv.put(START_DATE, currentDate);
        cv.put(START_TIME, dateFormat.format(new Date()));

        String condition = USER_NAME + " = '" + history.getUsername() + "' and "
                + START_DATE + " = '" + history.getStartDate() + "' and "
                + START_TIME + " = '" + history.getStartTime() + "' and "
                + MEDICATION_NAME + " = '" + history.getMedicationName() + "'";
        return getDatabase().getWritableDatabase().update(TABLE_NAME_HISTORY, cv, condition, null);

    }

    public static History getHistoryFromCursor(Cursor cursor) {

        return
                new History(
                        cursor.getString(cursor.getColumnIndex(MEDICATION_NAME)),
                        cursor.getString(cursor.getColumnIndex(USER_NAME)),
                        cursor.getString(cursor.getColumnIndex(START_DATE)),
                        cursor.getString(cursor.getColumnIndex(START_TIME)),
                        cursor.getInt(cursor.getColumnIndex(PILL_TAKEN))
                );
    }

    public static Cursor getDosageHistoryByPillWithId(String user_name, String date) {

        String condition = USER_NAME + " = '" + user_name + "' and " + START_DATE + " <= '" + date + "'";
        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME_MEDICATION, null, condition, null, MEDICATION_NAME, null, null);
        return cursor;
    }

    public static Cursor getDosageHistoryPillWithIdAndCurrentDate(String medName, String user_name, String date) {
        String condition = USER_NAME + " = '" + user_name + "' and " + START_DATE + " <= '" + date + "' and " + MEDICATION_NAME + " = '" + medName + "'";
        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME_HISTORY, null, condition, null, null, null, START_DATE + "," + START_TIME, null);
        return cursor;
    }

    public static Cursor getUpcomingSelfMedication(String user_name) {

        String condition = USER_NAME + " = '" + user_name + "'";
        condition += " and " + SCHEDULE + " = " + 0;

        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME_MEDICATION, null, condition, null, null, null, null, null);

        return cursor;

    }



    public static Cursor getSchedule(String user_name, String currentDate) {
        String condition = USER_NAME + " = '" + user_name + "' and " + START_DATE + " = '" + currentDate + "'";

        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME_MEDICATION, null, condition, null, null, null, START_DATE + " , " + START_TIME, null);

        return cursor;
    }

    public static Cursor getScheduleWeekly(String user_name, String dateBefore, String date) {
        String condition = USER_NAME + " = '" + user_name + "' and " + START_DATE + " >= '" + dateBefore + "'" + " and " + START_DATE + " <= '" + date + "'";

        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME_MEDICATION, null, condition, null, null, null, START_DATE + "," + START_TIME, null);

        return cursor;
    }

    public static boolean medicationRecordExists(MedicationData md) {
        String condition = USER_NAME + " = '" + md.getUsername() + "' and " +
                MEDICATION_NAME + " = '" + md.getMedicationName() + "'" + " and " +
                START_DATE + " = '" + md.getStartDate() + "'" + " and " +
                START_TIME + " = '" + md.getStartTime() + "'";
        Cursor cursor = getDatabase().getReadableDatabase().query(TABLE_NAME_MEDICATION, null, condition, null, null, null, null, null);

        //MyApplication.p("Count = " + cursor.getCount());
        return cursor.getCount() == 1;
    }

    public static void deleteMedicationFromDatabase(MedicationData md) {
        String condition = USER_NAME + " = '" + md.getUsername() + "' and " +
                MEDICATION_NAME + " = '" + md.getMedicationName() + "'" + " and " +
                START_DATE + " = '" + md.getStartDate() + "'" + " and " +
                START_TIME + " = '" + md.getStartTime() + "'";

        getDatabase().getReadableDatabase().delete(TABLE_NAME_MEDICATION, condition, null);
        getDatabase().getReadableDatabase().delete(TABLE_NAME_HISTORY, condition, null);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLE_CREATE_USER);
        sqLiteDatabase.execSQL(TABLE_CREATE_MEDICATION);
        sqLiteDatabase.execSQL(TABLE_CREATE_HISTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        Log.i(TAG, "Upgrading from Version " + i + " to Version " + i1);
        onCreate(sqLiteDatabase);
    }
}

