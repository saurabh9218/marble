package com.example.ektagomitbharani.marble;

/**
 * Created by Sau on 6/19/15.
 */

public class User {

    private String USER_NAME;
    private String PASSWORD;
    private String FIRST_NAME;
    private String LAST_NAME;
    private String EMAIL;
    private String PHONE;

    public User(String USER_NAME, String PASSWORD, String FIRST_NAME, String LAST_NAME, String EMAIL, String PHONE) {
        this.USER_NAME = USER_NAME;
        this.PASSWORD = PASSWORD;
        this.FIRST_NAME = FIRST_NAME;
        this.LAST_NAME = LAST_NAME;
        this.EMAIL = EMAIL;
        this.PHONE = PHONE;
    }

    public User(String USER_NAME, String PASSWORD) {
        this.USER_NAME = USER_NAME;
        this.PASSWORD = PASSWORD;
    }


    public String getUSER_NAME() {
        return USER_NAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public String getPHONE() {
        return PHONE;
    }
}
