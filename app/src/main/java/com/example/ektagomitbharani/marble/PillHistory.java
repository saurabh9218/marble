package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class PillHistory extends Activity {

    PillHistoryArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pill_history);

        ListView listView = (ListView) findViewById(R.id.pills_list_view);
        adapter = new PillHistoryArrayAdapter(this, R.layout.pill_history_list_item, null, 0);
        listView.setAdapter(adapter);

        String medicationName = getIntent().getStringExtra(Constants.MEDICATION_NAME);
        ((TextView) findViewById(R.id.pill_name)).setText(medicationName);

        new PillHistoryAsync().execute(medicationName);

    }

    protected class PillHistoryAsync extends AsyncTask<String, Void, Cursor> {

        @Override
        protected Cursor doInBackground(String... medName) {
            if (medName.length == 1) {
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);

                String date = sdf.format(new Date());
                return Database.getDosageHistoryPillWithIdAndCurrentDate(medName[0], MyApplication.getUser().getUSER_NAME(), date);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {

            adapter.swapCursor(cursor);
        }
    }


}
