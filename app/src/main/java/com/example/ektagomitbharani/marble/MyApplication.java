package com.example.ektagomitbharani.marble;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sau on 6/19/15.
 */
public class MyApplication extends Application {

    public static int id;
    private static MyApplication context;
    private static User user;
    private static MedicationData md;
    private static List<String> data = new ArrayList<String>();
    private static boolean isAutomatic;

    public static boolean isAutomatic() {
        return isAutomatic;
    }

    public static void setIsAutomatic(boolean isAutomatic) {
        MyApplication.isAutomatic = isAutomatic;
    }

    public static List<String> getData() {
        return data;
    }

    public static void setData(List<String> data) {
        MyApplication.data = data;
    }

    public static MedicationData getMd() {
        return md;
    }

    public static void setMd(MedicationData md) {
        MyApplication.md = md;
    }

    public static MyApplication getContext() {
        return context;
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        MyApplication.user = user;
    }

    public static void p(String str) {
        Log.i("Ekta Bharani", str + "");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        //Log.i("Saurabh Jain", Database.TABLE_CREATE_MEDICATION + "");
    }


}
