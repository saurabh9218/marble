package com.example.ektagomitbharani.marble;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Sau on 7/13/15.
 */
public class MyAlarmService extends Service {

    private NotificationManager mManager;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mManager = (NotificationManager) this.getApplicationContext().getSystemService(this.getApplicationContext().NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(this.getApplicationContext(), Home.class);
        intent1.setAction(Long.toString(System.currentTimeMillis()));

        if (intent.getIntExtra(Constants.NOTIFICATION_ALARM, 0) == 1) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");

            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(intent.getStringExtra(Constants.START_DATE)));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            int id = MyApplication.id++;
            PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this.getApplicationContext(), 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification notification = new Notification.Builder(this.getApplicationContext())
                    .setContentTitle("Dosage Reminder!")
                    .setContentText("subject")
                    .setSmallIcon(android.R.drawable.ic_lock_idle_alarm)
                    .setWhen(c.getTimeInMillis())
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setContentIntent(pendingNotificationIntent)
                    .setStyle(new Notification.BigTextStyle()
                            .bigText("You have to take \" + intent.getStringExtra(Constants.MEDICATION_NAME) + \" now!\""))
                    .build();
            intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            notification.flags |= Notification.FLAG_AUTO_CANCEL;


            mManager.notify(0, notification);
        }

        if (intent.getIntExtra(Constants.NOTIFICATION_EMAIL, 0) == 1) {

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"saurabhjain.kjsce@gmail.com"});
            i.putExtra(Intent.EXTRA_SUBJECT, "Dosage Reminder");
            i.putExtra(Intent.EXTRA_TEXT, "You have to take " + intent.getStringExtra(Constants.MEDICATION_NAME) + " now!");
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }

        }

        return START_NOT_STICKY;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);


    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}
