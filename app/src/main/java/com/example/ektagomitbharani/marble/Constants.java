package com.example.ektagomitbharani.marble;

/**
 * Created by Sau on 6/21/15.
 */
public class Constants {

    public static final String MEDICATION_NAME = "medication_name";
    public static final String USERNAME = "_id";
    public static final String MG = "mg";
    public static final String SCHEDULE = "schedule";

    public static final String PILL_QTY = "no_of_pills";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String NEVER_ENDING = "never_ending";
    public static final String START_TIME = "start_time";
    public static final String FREQUENCY = "frequency";
    public static final String NOTIFICATION_ALARM = "alarm";


    public static final String PILL_TAKEN = "pill_taken";
    public static final String NOTIFICATION_EMAIL = "email";

    public static class StartProgress {
        ;
    }

    public static class StopProgress {
        ;
    }
}
