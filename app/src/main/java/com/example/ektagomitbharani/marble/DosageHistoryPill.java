package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class DosageHistoryPill extends Activity {

    private DosageHistoryPillArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosage_history_pill);
        ListView listView = (ListView) findViewById(R.id.history_by_pills_list_view);
        adapter = new DosageHistoryPillArrayAdapter(this, R.layout.history_pill_list_item, null, 0);
        listView.setAdapter(adapter);

        new DosageHistoryPillAsync().execute();
    }

    protected class DosageHistoryPillAsync extends AsyncTask<Void, Void, Cursor> {


        @Override
        protected Cursor doInBackground(Void... voids) {

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);

            String date = sdf.format(new Date());

            return Database.getDosageHistoryByPillWithId(MyApplication.getUser().getUSER_NAME(), date);

        }

        @Override
        protected void onPostExecute(Cursor cursor) {

            adapter.swapCursor(cursor);
        }
    }


}
