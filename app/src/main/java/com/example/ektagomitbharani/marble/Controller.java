package com.example.ektagomitbharani.marble;

import android.os.AsyncTask;
import android.util.Log;

import de.greenrobot.event.EventBus;

/**
 * Created by Sau on 6/26/15.
 */
public class Controller {


    public static class StartProgress {
        ;
    }

    public static class StopProgress {
        ;
    }

    public static class Error {
        ;
    }

    public static void authenticateUser(User user) {

        EventBus.getDefault().post(new Controller.StartProgress());
        new CheckUserAsync().execute(user);
    }

    protected static class CheckUserAsync extends AsyncTask<User, Void, Void> {

        @Override
        protected Void doInBackground(User... users) {

            User user = null;

            if (users.length == 1)
                user = Database.checkUserWithId(users[0]);

            else
                Log.e("Ekta Bharani", "Bad number of arguments : required 1 found " + users.length);


            EventBus.getDefault().post(new Controller.StopProgress());

            if (user != null)
                EventBus.getDefault().post(user);
            else
                EventBus.getDefault().post(new Controller.Error());


            return null;
        }

    }
}
