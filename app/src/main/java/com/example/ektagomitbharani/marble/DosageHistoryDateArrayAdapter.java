package com.example.ektagomitbharani.marble;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

/**
 * Created by Sau on 6/28/15.
 */
public class DosageHistoryDateArrayAdapter extends ResourceCursorAdapter {


    public DosageHistoryDateArrayAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        History userHistory = Database.getHistoryFromCursor(cursor);

        TextView medication = (TextView) view.findViewById(R.id.dosage_date_name);
        TextView date = (TextView) view.findViewById(R.id.dosage_date);
        final ToggleButton toggleButton = (ToggleButton) view.findViewById(R.id.toggleButton);

        int i = cursor.getInt(cursor.getColumnIndex(Constants.PILL_TAKEN));

        if (i == 1) {
            toggleButton.setChecked(true);
        } else {
            toggleButton.setChecked(false);
        }

        toggleButton.setTag(userHistory);
        date.setText("" + cursor.getString(cursor.getColumnIndex(Constants.START_DATE)));
        medication.setText("" + cursor.getString(cursor.getColumnIndex(Constants.MEDICATION_NAME)));

        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int value;

                if (toggleButton.isChecked())
                    value = 1;
                else
                    value = 0;
                History history = (History) view.getTag();
                history.update(value);

            }
        });
    }


}
