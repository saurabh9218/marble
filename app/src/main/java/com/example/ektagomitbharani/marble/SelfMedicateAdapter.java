package com.example.ektagomitbharani.marble;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

/**
 * Created by Sau on 7/8/15.
 */
public class SelfMedicateAdapter extends ResourceCursorAdapter {

    public SelfMedicateAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        MedicationData data = Database.getMedicationData(cursor);
        view.setTag(data);
        ((TextView) view.findViewById(R.id.self_med_name)).setText(cursor.getString(cursor.getColumnIndex(Constants.MEDICATION_NAME)));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MedicationData md = (MedicationData) view.getTag();
                History history =
                        new History(md.getMedicationName(), MyApplication.getUser().getUSER_NAME(), md.getStartDate(), md.getStartTime(), 1);
                history.updateForSelfMedicate();
                //md.delete()
            }
        });

    }
}
