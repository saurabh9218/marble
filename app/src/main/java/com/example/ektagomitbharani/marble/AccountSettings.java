package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import de.greenrobot.event.EventBus;


public class AccountSettings extends Activity {


    private User currentUser;
    private EditText fname;
    private EditText lname;
    private EditText username;
    private EditText password;
    private EditText phone;
    private EditText email;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setContentView(R.layout.activity_account_settings);
        fname = (EditText) findViewById(R.id.fname);
        lname = (EditText) findViewById(R.id.lname);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        phone = (EditText) findViewById(R.id.phone_no);
        email = (EditText) findViewById(R.id.email);

        currentUser = MyApplication.getUser();


        fname.setText("" + currentUser.getFIRST_NAME());
        lname.setText("" + currentUser.getLAST_NAME());
        username.setText("" + currentUser.getUSER_NAME());
        password.setText("" + currentUser.getPASSWORD());
        // Log.i("Ekta ",currentUser.getPHONE()+"");
        phone.setText("" + currentUser.getPHONE());
        email.setText("" + currentUser.getEMAIL());


        findViewById(R.id.settings_finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateValues();
            }
        });

    }

    private void updateValues() {

        String firstName = fname.getText().toString();
        String lastName = lname.getText().toString();
        String uname = username.getText().toString();
        String pwd = password.getText().toString();
        String ph_num = phone.getText().toString();
        String email_id = email.getText().toString();
        // Log.i("Ekta Bharani","pwd : Seetings: "+pwd);
        User user = new User(uname, pwd, firstName, lastName, ph_num, email_id);
        AccountController.updatUser(user);

    }


    public void onEventMainThread(AccountController.StartProgress start) {

        progressDialog = ProgressDialog.show(AccountSettings.this, getString(R.string.progress_title), getString(R.string.updating));
    }

    public void onEventMainThread(AccountController.StopProgress stop) {
        progressDialog.dismiss();
    }

    public void onEventMainThread(AccountController.Error error) {
        new AlertDialog.Builder(AccountSettings.this)
                .setTitle(R.string.error)
                .setMessage(R.string.update_error)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    public void onEventMainThread(final User user) {
        new AlertDialog.Builder(AccountSettings.this)
                .setTitle(R.string.success)
                .setMessage(R.string.update_success)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MyApplication.setUser(user);
                        startActivity(new Intent(AccountSettings.this, Home.class));
                        finish();
                    }
                })
                .show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
