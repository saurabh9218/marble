package com.example.ektagomitbharani.marble;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

/**
 * Created by Sau on 7/6/15.
 */
public class PillHistoryArrayAdapter extends ResourceCursorAdapter {

    public PillHistoryArrayAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        History data = Database.getHistoryFromCursor(cursor);
        TextView pillDate = (TextView) view.findViewById(R.id.pill_date);
        TextView pilTime = (TextView) view.findViewById(R.id.pill_time);

        pillDate.setText(data.getStartDate());
        pilTime.setText(data.getStartTime());

        CheckedTextView cb = (CheckedTextView) view.findViewById(R.id.pill_taken);

        if (data.getPillTaken() == 1)
            cb.setChecked(true);

    }
}
