package com.example.ektagomitbharani.marble;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Sau on 7/10/15.
 */
public class MonthArrayAdapter extends ArrayAdapter {
    private final Context context;
    private final List<MedicationData> data;

    public MonthArrayAdapter(Context context, List<MedicationData> data) {
        super(context, 0, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        MedicationData data = (MedicationData) getItem(position);

        if (view == null)
            view = LayoutInflater.from(context).inflate(R.layout.activity_schedule_adapter, parent, false);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView qty = (TextView) view.findViewById(R.id.qty);
        TextView time = (TextView) view.findViewById(R.id.time);
        TextView pills = (TextView) view.findViewById(R.id.pills);
        TextView date = (TextView) view.findViewById(R.id.date);

        name.setText(data.getMedicationName());
        qty.setText(data.getMg() + "");
        time.setText(data.getStartTime());
        pills.setText(data.getNoOfPills() + "");
        date.setText(data.getStartDate());

        return view;
    }
}
