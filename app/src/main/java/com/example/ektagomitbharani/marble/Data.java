package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class Data extends Activity implements View.OnClickListener {

    final Calendar c = Calendar.getInstance();
    private CheckBox cb;
    private EditText mydate;
    private EditText time;
    private EditText end;
    private int frequency;
    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        end = (EditText) findViewById(R.id.end_date);
        mydate = (EditText) findViewById(R.id.date);
        time = (EditText) findViewById(R.id.time);
        cb = (CheckBox) findViewById(R.id.never_ending);

        final TextView tv = (TextView) findViewById(R.id.end_date_textview);

        final Spinner freq = (Spinner) findViewById(R.id.frequency);
        frequency = 1;

        final TextView frequencyTv = (TextView) findViewById(R.id.frequency_text);
        frequencyTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFrequency();

            }

            private void getFrequency() {
                dialog = new Dialog(Data.this);
                dialog.setContentView(R.layout.custom_frequency_dialog);
                dialog.setTitle("Choose Frequency");

                TextView tv1 = (TextView) dialog.findViewById(R.id.textView9);
                TextView tv2 = (TextView) dialog.findViewById(R.id.textView10);
                TextView tv3 = (TextView) dialog.findViewById(R.id.textView11);
                TextView tv4 = (TextView) dialog.findViewById(R.id.textView12);
                TextView tv5 = (TextView) dialog.findViewById(R.id.textView13);
                TextView tv6 = (TextView) dialog.findViewById(R.id.textView14);

                tv1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        frequency = 1;
                        frequencyTv.setText("Frequency : Once Daily");
                        dialog.dismiss();
                    }
                });
                tv2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        frequency = 2;
                        frequencyTv.setText("Frequency : Twice Daily");
                        dialog.dismiss();
                    }
                });
                tv3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        frequency = 3;
                        frequencyTv.setText("Frequency : 3x Daily");
                        dialog.dismiss();
                    }
                });
                tv4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        frequency = 4;
                        frequencyTv.setText("Frequency : 4x Daily");
                        dialog.dismiss();
                    }
                });
                tv5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        frequency = 5;
                        frequencyTv.setText("Frequency : 5x Daily");
                        dialog.dismiss();
                    }
                });
                tv6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        frequency = 6;
                        frequencyTv.setText("Frequency : 6x Daily");
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }

        });

        /*
        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.freq_entries));

        freq.setAdapter(adapter);

        freq.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView selectedText = (TextView) parent.getChildAt(0);
                switch (selectedText.getText().toString()) {
                    case "Once Daily":
                        frequency = 1;
                        break;
                    case "Twice Daily":
                        frequency = 2;
                        break;
                    case "3x Daily":
                        frequency = 3;
                        break;
                    case "4x Daily":
                        frequency = 4;
                        break;
                    case "5x Daily":
                        frequency = 5;
                        break;
                    case "6x Daily":
                        frequency = 6;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
*/

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    tv.setVisibility(View.GONE);
                    end.setVisibility(View.GONE);
                } else {
                    tv.setVisibility(View.VISIBLE);
                    end.setVisibility(View.VISIBLE);
                }
            }
        });

        findViewById(R.id.data_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processData();

            }
        });
        mydate.setOnClickListener(this);
        time.setOnClickListener(this);
        end.setOnClickListener(this);
    }

    private void processData() {


        EditText pills = (EditText) findViewById(R.id.pills);
        String endDate;

        int neverEnding;

        if (cb.isChecked()) {
            endDate = null;
            neverEnding = 1;
        } else {
            endDate = end.getText().toString();
            neverEnding = 0;
        }


        String pillQty = pills.getText().toString();
        String startDate = mydate.getText().toString();
        String startTime = time.getText().toString();

        if (pillQty.isEmpty() || startDate.isEmpty() || startTime.isEmpty() || (!cb.isChecked() && endDate.isEmpty())) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(Data.this);
            dialog.setMessage("Please fill all the details and proceed!")
                    .setTitle("Alert!")
                    .setPositiveButton(android.R.string.ok, null);

            AlertDialog dialogBox = dialog.create();
            dialogBox.show();
        } else {

            Intent intent = getIntent();
            String medicationName = intent.getStringExtra(Constants.MEDICATION_NAME);
            String username = intent.getStringExtra(Constants.USERNAME);
            int mgQty = intent.getIntExtra(Constants.MG, 0);
            boolean isSchedule = intent.getBooleanExtra(Constants.SCHEDULE, true);

            Intent newIntent = new Intent(Data.this, MoreData.class);

            int intPills = Integer.parseInt(pillQty);


            final MedicationData md = new MedicationData();
            md.setMedicationName(medicationName);
            md.setUsername(username);
            md.setMg(mgQty);
            md.setSchedule(isSchedule == true ? 1 : 0);
            md.setNoOfPills(intPills);
            md.setStartDate(startDate);
            md.setEndTime(endDate);
            md.setNeverEnding(neverEnding);
            md.setStartTime(startTime);
            md.setFrequency(frequency);

            MyApplication.setMd(md);


            /*newIntent.putExtra(Constants.MEDICATION_NAME, medicationName);
            newIntent.putExtra(Constants.USERNAME, username);
            newIntent.putExtra(Constants.MG, mgQty);
            newIntent.putExtra(Constants.SCHEDULE, isSchedule);

            newIntent.putExtra(Constants.PILL_QTY, intPills);
            newIntent.putExtra(Constants.START_DATE, startDate);
            newIntent.putExtra(Constants.END_DATE, endDate);
            newIntent.putExtra(Constants.NEVER_ENDING, neverEnding);
            newIntent.putExtra(Constants.START_TIME, startTime);
            newIntent.putExtra(Constants.FREQUENCY, frequency);
            */
            newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(newIntent);

        }

    }


    @Override
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.time:
                getTime();
                break;
            case R.id.date:
                getDate();
                break;
            case R.id.end_date:
                getEndDate();
                break;
            case R.id.textView9:
                frequency = 1;
                dialog.dismiss();
                break;
            case R.id.textView10:
                frequency = 2;
                dialog.dismiss();
                break;
            case R.id.textView11:
                frequency = 3;
                dialog.dismiss();
                break;
            case R.id.textView12:
                frequency = 4;
                dialog.dismiss();
                break;
            case R.id.textView13:
                frequency = 5;
                dialog.dismiss();
                break;
            case R.id.textView14:
                frequency = 6;
                dialog.dismiss();
                break;

        }
    }

    private void getTime() {


        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        int hour = hourOfDay % 12;
                        if (hour == 0)
                            hour = 12;
                        time.setText(String.format("%02d:%02d %s", hour, minute, hourOfDay < 12 ? "AM" : "PM"));
                        //time.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        tpd.show();

    }

    private void getDate() {

        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);

        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        c.set(Calendar.YEAR, year);
                        c.set(Calendar.MONTH, monthOfYear);
                        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        mydate.setText(sdf.format(c.getTime()));

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    private void getEndDate() {

        final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);

        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        c.set(Calendar.YEAR, year);
                        c.set(Calendar.MONTH, monthOfYear);
                        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        end.setText(sdf.format(c.getTime()));

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }
}
