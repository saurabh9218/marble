package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.greenrobot.event.EventBus;


public class Home extends Activity implements View.OnClickListener {

    CustomHomeListViewAdapter adapter;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        EventBus.getDefault().register(this);

        ListView listView = (ListView) findViewById(R.id.list_view);
        adapter = new CustomHomeListViewAdapter(getApplicationContext(), R.layout.list_item_home, null, 0);

        listView.setAdapter(adapter);
        new ListMedication().execute();

        findViewById(R.id.add_med).setOnClickListener(this);
        findViewById(R.id.logout).setOnClickListener(this);
        findViewById(R.id.settings).setOnClickListener(this);
        findViewById(R.id.history).setOnClickListener(this);
        findViewById(R.id.self_medicate).setOnClickListener(this);
        findViewById(R.id.view_schedule).setOnClickListener(this);
        findViewById(R.id.drug_info).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.add_med:
                startActivity(new Intent(Home.this, Medication.class));
                break;
            case R.id.logout:
                logout();
                break;
            case R.id.settings:
                settings();
                break;
            case R.id.history:
                history();
                break;
            case R.id.self_medicate:
                selfMedicate();
                break;
            case R.id.view_schedule:
                viewSchedule();
                break;
            case R.id.drug_info:
                drugInfo();
                break;
        }
    }

    private void drugInfo() {
        Intent intent = new Intent(Home.this, DrugInfo.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    private void viewSchedule() {
        startActivity(new Intent(Home.this, ViewSchedule.class));
    }

    private void selfMedicate() {
        startActivity(new Intent(Home.this, SelfMedicate.class));
    }

    private void history() {

        startActivity(new Intent(Home.this, DosageHistory.class));
    }

    private void settings() {
        startActivity(new Intent(Home.this, AccountSettings.class));
    }

    private void logout() {
        MyApplication.setUser(null);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(Constants.StartProgress startProgress) {
        progressDialog = ProgressDialog.show(this, "Loading...", "Please Wait!");
    }

    public void onEventMainThread(Constants.StopProgress stopProgress) {
        progressDialog.dismiss();
    }

    public void onEventMainThread(Cursor cursor) {
        adapter.swapCursor(cursor);
    }


    protected class ListMedication extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected void onPreExecute() {

            EventBus.getDefault().post(new Constants.StartProgress());
        }

        @Override
        protected Cursor doInBackground(Void... voids) {

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
            String currentDate = sdf.format(new Date());
            Date current = new Date();
            try {
                current = sdf.parse(currentDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Cursor cursor = Database.getUpcomingDosagesWithId(MyApplication.getUser().getUSER_NAME(), currentDate);
            while (cursor.moveToNext()) {

                MedicationData md = Database.getMedicationData(cursor);


                String today = sdf.format(new Date());

                Date other = new Date();
                try {
                    other = sdf.parse(md.getStartDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar c = Calendar.getInstance();
                long days = getDays(current, other);


                if (days >= 0 && md.getNeverEnding() == 1) {
                    for (int i = 0; i < 2; i++) {

                        try {
                            c.setTime(sdf.parse(today));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        c.add(Calendar.DATE, 1);
                        today = sdf.format(c.getTime());
                        MedicationData temp = new MedicationData(md);
                        temp.setStartDate(today);
                        if (!temp.medicationExists())
                            Database.addMedication(temp);
                    }

                } else if (md.getNeverEnding() == 0) {

                    Date start = new Date();
                    Date end = new Date();

                    try {
                        start = sdf.parse(md.getStartDate());
                        end = sdf.parse(md.getEndTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    days = getDays(start, end);

                    for (int i = 0; i < days; i++) {
                        c.setTime(start);
                        c.add(Calendar.DATE, 1);
                        today = sdf.format(c.getTime());
                        MedicationData temp = new MedicationData(md);
                        temp.setStartDate(today);
                        Database.addMedication(temp);
                        try {
                            start = sdf.parse(today);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }


            }

            return Database.getUpcomingDosagesWithId(MyApplication.getUser().getUSER_NAME(), currentDate);
        }


        private long getDays(Date currentDate, Date otherDate) {


            Calendar calendar1 = Calendar.getInstance();
            Calendar calendar2 = Calendar.getInstance();
            calendar1.setTime(currentDate);
            calendar2.setTime(otherDate);
            long milliseconds1 = calendar1.getTimeInMillis();
            long milliseconds2 = calendar2.getTimeInMillis();
            long diff = milliseconds2 - milliseconds1;
            return diff / (24 * 60 * 60 * 1000);

        }


        @Override
        protected void onPostExecute(Cursor cursor) {

            EventBus.getDefault().post(new Constants.StopProgress());
            EventBus.getDefault().post(cursor);
            //adapter.swapCursor(cursor);

        }
    }
}
