package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;


public class DosageTime extends Activity implements View.OnClickListener {

    String times[];
    EditText[] editTexts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosage_time);

        int frequency = MyApplication.getMd().getFrequency();

        times = new String[frequency];
        editTexts = new EditText[6];

        editTexts[0] = (EditText) findViewById(R.id.dosage_time1);
        editTexts[1] = (EditText) findViewById(R.id.dosage_time2);
        editTexts[2] = (EditText) findViewById(R.id.dosage_time3);
        editTexts[3] = (EditText) findViewById(R.id.dosage_time4);
        editTexts[4] = (EditText) findViewById(R.id.dosage_time5);
        editTexts[5] = (EditText) findViewById(R.id.dosage_time6);

        for (int i = 0; i < frequency; i++) {
            editTexts[i].setVisibility(View.VISIBLE);
            editTexts[i].setOnClickListener(this);
        }

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printTimes();
            }
        });
    }

    @Override
    public void onClick(final View v) {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog tpd = new TimePickerDialog(DosageTime.this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        int hour = hourOfDay % 12;
                        if (hour == 0)
                            hour = 12;
                        String t = String.format("%02d:%02d %s", hour, minute, hourOfDay < 12 ? "AM" : "PM");
                        ((EditText) v).setText(t);
                    }
                }, mHour, mMinute, false);
        tpd.show();

    }

    private void printTimes() {
        MedicationData md[] = new MedicationData[times.length];
        for (int i = 0; i < times.length; i++) {
            md[i] = new MedicationData(MyApplication.getMd());
            times[i] = editTexts[i].getText().toString();
            md[i].setStartTime(times[i]);
        }

        MedicationData.addCustomMedication(md);

    }
}
