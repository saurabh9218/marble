package com.example.ektagomitbharani.marble;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

/**
 * Created by Sau on 6/22/15.
 */
public class CustomHomeListViewAdapter extends ResourceCursorAdapter {

    public CustomHomeListViewAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {


        MedicationData md = Database.getMedicationData(cursor);
        view.setTag(md);
        TextView name = (TextView) view.findViewById(R.id.med_name);
        TextView date = (TextView) view.findViewById(R.id.med_date);
        TextView time = (TextView) view.findViewById(R.id.med_time);

        date.setText("" + cursor.getString(cursor.getColumnIndex(Constants.START_DATE)));

        time.setText("" + cursor.getString(cursor.getColumnIndex(Constants.START_TIME)));
        name.setText("" + cursor.getString(cursor.getColumnIndex(Constants.MEDICATION_NAME)));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MedicationData data = (MedicationData) view.getTag();
                data.deleteMedication();
            }
        });

    }
}
