package com.example.ektagomitbharani.marble;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class DrugInfo extends AppCompatActivity implements AdapterView.OnItemClickListener {


    private ArrayList<String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_info);
        ListView drugList = (ListView) findViewById(R.id.drug_info_list);
        data = new ArrayList<>();
        getData();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, data);
        drugList.setAdapter(adapter);
        drugList.setOnItemClickListener(this);

    }

    private void getData() {
        data.add("tylenol");
        data.add("aspirin");
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String drug = data.get(position);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://www.drugs.com/" + drug));
        startActivity(intent);
    }
}
