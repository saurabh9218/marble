package com.example.ektagomitbharani.marble;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.List;

/**
 * Created by saurabh on 8/12/15.
 */
public class DosageTimeAdapter extends ArrayAdapter {

    final Calendar c = Calendar.getInstance();
    private final List<String> data;
    private final Context context;

    public DosageTimeAdapter(Context context, List<String> data) {
        super(context, 0, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.dosage_list_item, parent, false);

        String s = data.get(position);
        final EditText time = (EditText) convertView.findViewById(R.id.dosage_time);
        time.setHint(s + "");
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);

                TimePickerDialog tpd = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                int hour = hourOfDay % 12;
                                if (hour == 0)
                                    hour = 12;
                                String t = String.format("%02d:%02d %s", hour, minute, hourOfDay < 12 ? "AM" : "PM");
                                time.setText(t);
                                MyApplication.getData().remove(position);
                                MyApplication.getData().add(position, t);
                                //MyApplication.p(MyApplication.getData().get(position).toString());

                            }
                        }, mHour, mMinute, false);
                tpd.show();
            }


        });

        return convertView;
    }

    private void getTime() {


    }
}
