package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ListView;
import android.widget.TextView;

import de.greenrobot.event.EventBus;


public class SelfMedicate extends Activity {

    private SelfMedicateAdapter adapter;
    private TextView recorded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_medicate);
        EventBus.getDefault().register(this);

        recorded = (TextView) findViewById(R.id.recorded);
        recorded.setText("");
        ListView listView = (ListView) findViewById(R.id.self_list_view);

        adapter = new SelfMedicateAdapter(this, R.layout.list_self_medicate, null, 0);

        listView.setAdapter(adapter);
        new SelfMedication().execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(Long id) {

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
                //recorded.setText("seconds remaining: " + millisUntilFinished / 1000);
                recorded.setText("Your dosage has been recorded!");
            }

            public void onFinish() {
                recorded.setText("");
            }
        }.start();

    }

    protected class SelfMedication extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(Void... voids) {

            return Database.getUpcomingSelfMedication(MyApplication.getUser().getUSER_NAME());

        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            adapter.swapCursor(cursor);
        }
    }


}
