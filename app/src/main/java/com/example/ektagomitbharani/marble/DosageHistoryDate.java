package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class DosageHistoryDate extends Activity {

    private DosageHistoryDateArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosage_history_date);

        ListView listView = (ListView) findViewById(R.id.pills_list_view);
        adapter = new DosageHistoryDateArrayAdapter(this, R.layout.history_date_list_item, null, 0);
        listView.setAdapter(adapter);
        new DosageHistoryDateAsync().execute();

    }

    protected class DosageHistoryDateAsync extends AsyncTask<Void, Void, Cursor> {


        @Override
        protected Cursor doInBackground(Void... voids) {

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);

            String date = sdf.format(new Date());
            return Database.getDosageHistoryDateWithIdAndCurrentDate(MyApplication.getUser().getUSER_NAME(), date);

        }

        @Override
        protected void onPostExecute(Cursor cursor) {

            adapter.swapCursor(cursor);
        }
    }


}
