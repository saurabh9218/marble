package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class DosageHistory extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosage_history);
        findViewById(R.id.history_pill).setOnClickListener(this);
        findViewById(R.id.history_date).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.history_pill:
                getHistoryByPill();
                break;
            case R.id.history_date:
                getHistoryByDate();
                break;
        }
    }

    private void getHistoryByDate() {

        startActivity(new Intent(DosageHistory.this, DosageHistoryDate.class));

    }

    private void getHistoryByPill() {
        startActivity(new Intent(DosageHistory.this, DosageHistoryPill.class));
    }
}
