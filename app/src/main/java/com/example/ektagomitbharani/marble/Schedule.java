package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class Schedule extends Activity {
    private ScheduleAdapter adapter;
    private MonthArrayAdapter monthArrayAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        String s = getIntent().getExtras().getString("Schedule");

        TextView title = (TextView) findViewById(R.id.schedule_title);
        listView = (ListView) findViewById(R.id.scheduleView);

        if ("Today".equals(s)) {
            title.setText("Today's Schedule");
            new ListSchedule().execute();
        } else if ("Tomorrow".equals(s)) {
            title.setText("Tomorrow's Schedule");
            new ListScheduleTom().execute();
        } else if ("Weekly".equals(s)) {
            title.setText("Weekly Schedule");
            new ListScheduleWeek().execute();
        } else if ("Monthly".equals(s)) {
            title.setText("Monthly Schedule");
            new ListScheduleMonth().execute();
        }
    }

    protected class ListSchedule extends AsyncTask<Void, Void, Cursor> {
        @Override
        protected Cursor doInBackground(Void... Voids) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
            String currentDate = sdf.format(new Date());
            Cursor cursor;
            cursor = Database.getSchedule(MyApplication.getUser().getUSER_NAME(), currentDate);
            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {

            adapter = new ScheduleAdapter(Schedule.this, R.layout.activity_schedule_adapter, cursor, 0);

            listView.setAdapter(adapter);
        }

    }

    protected class ListScheduleTom extends AsyncTask<Void, Void, Cursor> {
        @Override
        protected Cursor doInBackground(Void... Voids) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
            String today = sdf.format(new Date());
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(today));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, 1);
            String currentDate = sdf.format(c.getTime());
            Cursor cursor;
            cursor = Database.getSchedule(MyApplication.getUser().getUSER_NAME(), currentDate);
            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {

            adapter = new ScheduleAdapter(Schedule.this, R.layout.activity_schedule_adapter, cursor, 0);

            listView.setAdapter(adapter);

        }

    }

    protected class ListScheduleWeek extends AsyncTask<Void, Void, Cursor> {
        @Override
        protected Cursor doInBackground(Void... Voids) {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
            String today = sdf.format(new Date());
            Calendar c = Calendar.getInstance();
            try {
                c.setTime(sdf.parse(today));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, 7);
            String currentDate = sdf.format(c.getTime());
            Cursor cursor;
            cursor = Database.getScheduleWeekly(MyApplication.getUser().getUSER_NAME(), today, currentDate);
            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {

            adapter = new ScheduleAdapter(Schedule.this, R.layout.activity_schedule_adapter, cursor, 0);

            listView.setAdapter(adapter);

        }

    }

    protected class ListScheduleMonth extends AsyncTask<Void, Void, List<MedicationData>> {

        @Override
        protected List<MedicationData> doInBackground(Void... voids) {

            List<MedicationData> data = new ArrayList<MedicationData>();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
            String currentDate = sdf.format(new Date());

            Calendar c = Calendar.getInstance();
            c.add(Calendar.MONTH, 1);

            Date nextMonth = c.getTime();

            Cursor cursor = Database.getUpcomingDosagesWithId(MyApplication.getUser().getUSER_NAME(), currentDate);

            while (cursor.moveToNext()) {
                MedicationData md = Database.getMedicationData(cursor);
                Date temp = new Date();
                try {
                    temp = sdf.parse(md.getStartDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long days = getDays(nextMonth, temp);
                if (days > 30) {
                    data.add(md);
                }
                MyApplication.p("" + days);
            }

            return data;
        }

        @Override
        protected void onPostExecute(List<MedicationData> data) {

            monthArrayAdapter = new MonthArrayAdapter(Schedule.this, data);
            listView.setAdapter(monthArrayAdapter);
        }

        private long getDays(Date currentDate, Date otherDate) {


            Calendar calendar1 = Calendar.getInstance();
            Calendar calendar2 = Calendar.getInstance();
            calendar1.setTime(currentDate);
            calendar2.setTime(otherDate);
            long milliseconds1 = calendar1.getTimeInMillis();
            long milliseconds2 = calendar2.getTimeInMillis();
            long diff = milliseconds2 - milliseconds1;
            return diff / (24 * 60 * 60 * 1000);

        }
    }


}
