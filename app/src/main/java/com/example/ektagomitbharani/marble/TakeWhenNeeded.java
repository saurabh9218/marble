package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


public class TakeWhenNeeded extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_when_needed);
        findViewById(R.id.finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TakeWhenNeeded.this, Home.class));
                finish();
            }
        });
    }


}
