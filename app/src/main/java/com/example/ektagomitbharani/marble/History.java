package com.example.ektagomitbharani.marble;

import android.os.AsyncTask;

import de.greenrobot.event.EventBus;

/**
 * Created by Sau on 6/28/15.
 */
public class History {

    private String medicationName;
    private String username;
    private String startDate;
    private String startTime;
    private int pillTaken;

    public History(String medicationName, String username, String startDate, String startTime, int pillTaken) {
        this.medicationName = medicationName;
        this.username = username;
        this.startDate = startDate;
        this.startTime = startTime;
        this.pillTaken = pillTaken;
    }

    public History() {
    }


    public String getMedicationName() {
        return medicationName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getPillTaken() {
        return pillTaken;
    }

    public void setPillTaken(int pillTaken) {
        this.pillTaken = pillTaken;
    }

    public void setMedicationID(String medicationName) {
        this.medicationName = medicationName;
    }

    public void update(int value) {

        this.setPillTaken(value);

        new UpdateHistory().execute(this);
    }

    public void delete() {
        //new DeleteHistory().execute(this);
    }

    public void updateForSelfMedicate() {

        new UpdateHistoryForSelfMedicate().execute(this);
    }

    protected class UpdateHistoryForSelfMedicate extends AsyncTask<History, Void, Long> {

        @Override
        protected Long doInBackground(History... history) {

            long success = -1;

            if (history.length == 1)
                success = Database.updateHistorySelfMedicateWithId(history[0]);
            if (success != -1)
                MyApplication.p("Success Self Medicate Update history");
            return success;
        }

        @Override
        protected void onPostExecute(Long id) {
            EventBus.getDefault().post(new Long(id));
        }
    }

    protected class UpdateHistory extends AsyncTask<History, Void, Void> {

        @Override
        protected Void doInBackground(History... history) {

            long success = -1;

            if (history.length == 1)
                success = Database.updateHistory(history[0]);

            if (success != -1)
                MyApplication.p("Update Success");

            return null;
        }
    }

}
