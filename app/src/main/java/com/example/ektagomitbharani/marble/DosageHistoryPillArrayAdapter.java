package com.example.ektagomitbharani.marble;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.Button;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

/**
 * Created by Sau on 6/29/15.
 */
public class DosageHistoryPillArrayAdapter extends ResourceCursorAdapter {


    private final Context context;

    public DosageHistoryPillArrayAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
        this.context = context;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {

        MedicationData md = Database.getMedicationData(cursor);
        TextView name = (TextView) view.findViewById(R.id.history_by_pill_name);
        name.setText(cursor.getString(cursor.getColumnIndex(Constants.MEDICATION_NAME)));

        view.findViewById(R.id.history_by_pill_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyApplication.p("Button Pressed");
            }
        });

        Button history = (Button) view.findViewById(R.id.history_by_pill_button);
        history.setTag(md);
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MedicationData data = (MedicationData) view.getTag();
                Intent intent = new Intent(context, PillHistory.class);
                intent.putExtra(Constants.MEDICATION_NAME, data.getMedicationName());
                context.startActivity(intent);
            }
        });
    }
}
