package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Medication extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medication);

        findViewById(R.id.medication_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processMedication();
            }
        });
    }

    private void processMedication() {

        RadioButton schedule = (RadioButton) findViewById(R.id.schedule);
        RadioButton needed = (RadioButton) findViewById(R.id.needed);

        EditText medication = (EditText) findViewById(R.id.medication_name);
        EditText mg = (EditText) findViewById(R.id.mg_per_pill);

        String medicationName = medication.getText().toString();
        String mgQty = mg.getText().toString();

        if (medicationName.isEmpty() || mgQty.isEmpty()) {


            AlertDialog.Builder dialog = new AlertDialog.Builder(Medication.this);
            dialog.setMessage("Please fill all the details and proceed!")
                    .setTitle("Alert!")
                    .setPositiveButton(android.R.string.ok, null);

            AlertDialog dialogBox = dialog.create();
            dialogBox.show();
        } else {

            int qty = Integer.parseInt(mgQty);
            if (needed.isChecked()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
                String currentDate = sdf.format(new Date());

                MedicationData data = new MedicationData(medicationName, MyApplication.getUser().getUSER_NAME(),
                        qty, 0, 0, currentDate, dateFormat.format(new Date()), null, 0, 0, 0, 0, 0
                );
                new NeededMedication().execute(data);

            } else if (schedule.isChecked()) {

                Intent intent = new Intent(Medication.this, Data.class);
                intent.putExtra(Constants.MEDICATION_NAME, medicationName);
                intent.putExtra(Constants.USERNAME, MyApplication.getUser().getUSER_NAME());
                intent.putExtra(Constants.MG, qty);
                intent.putExtra(Constants.SCHEDULE, schedule.isChecked());
                startActivity(intent);
            } else {
                AlertDialog.Builder dialog = new AlertDialog.Builder(Medication.this);
                dialog.setMessage("Something went wrong. Please try again later!")
                        .setTitle("Error!")
                        .setPositiveButton(android.R.string.ok, null);

                AlertDialog dialogBox = dialog.create();
                dialogBox.show();
            }
        }
    }


    protected class NeededMedication extends AsyncTask<MedicationData, Void, Void> {

        @Override
        protected Void doInBackground(MedicationData... data) {

            if (data.length == 1) {

                Database.addMedication(new MedicationData(
                        data[0].getMedicationName(),
                        data[0].getUsername(),
                        data[0].getMg(),
                        data[0].isSchedule(),
                        data[0].getNoOfPills(),
                        data[0].getStartDate(),
                        data[0].getStartTime(),
                        data[0].getEndTime(),
                        data[0].getNeverEnding(),
                        data[0].getFrequency(),
                        data[0].isNotificationAlarm(),
                        data[0].isNotificationText(),
                        data[0].isNotificationEmail()
                ));

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            startActivity(new Intent(Medication.this, TakeWhenNeeded.class));
            finish();
        }
    }
}
