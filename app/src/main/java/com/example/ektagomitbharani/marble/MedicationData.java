package com.example.ektagomitbharani.marble;

import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.greenrobot.event.EventBus;

/**
 * Created by Sau on 6/21/15.
 */
public class MedicationData {

    //private int medicationID;
    private String medicationName;
    private String username;
    private int mg;
    private int schedule;
    private int noOfPills;
    private String startDate;
    private String startTime;
    private String endTime;
    private int neverEnding;
    private int frequency;
    private int notificationAlarm;
    private int notificationText;
    private int notificationEmail;


    public MedicationData(String medicationName, String username, int mg, int schedule, int noOfPills,
                          String startDate, String startTime, String endTime, int neverEnding, int frequency,
                          int notificationAlarm, int notificationText, int notificationEmail) {

        this.medicationName = medicationName;
        this.username = username;
        this.mg = mg;
        this.schedule = schedule;
        this.noOfPills = noOfPills;
        this.startDate = startDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.neverEnding = neverEnding;
        this.frequency = frequency;
        this.notificationAlarm = notificationAlarm;
        this.notificationText = notificationText;
        this.notificationEmail = notificationEmail;
    }

    public MedicationData(String medicationName, String startDate, String startTime) {
        this.medicationName = medicationName;
        this.startDate = startDate;
        this.startTime = startTime;

    }

    public MedicationData(MedicationData md) {

        this.medicationName = md.getMedicationName();
        this.username = md.getUsername();
        this.mg = md.getMg();
        this.schedule = md.isSchedule();
        this.noOfPills = md.getNoOfPills();
        this.startDate = md.getStartDate();
        this.startTime = md.getStartTime();
        this.endTime = md.getEndTime();
        this.neverEnding = md.getNeverEnding();
        this.frequency = md.getFrequency();
        this.notificationAlarm = md.isNotificationAlarm();
        this.notificationText = md.isNotificationText();
        this.notificationEmail = md.isNotificationEmail();
    }

    public MedicationData() {

    }

   /*
    public int getMedicationID() {
        return medicationID;
    }
    */

    public static void addCustomMedication(MedicationData data[]) {
        new ScheduleCustomMedication().execute(data);
    }

    @Override
    public String toString() {

        return "start Time : " + startTime;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getMg() {
        return mg;
    }

    public void setMg(int mg) {
        this.mg = mg;
    }

    public int isSchedule() {
        return schedule;
    }

    public int getNoOfPills() {
        return noOfPills;
    }

    public void setNoOfPills(int noOfPills) {
        this.noOfPills = noOfPills;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int isNotificationAlarm() {
        return notificationAlarm;
    }

    public int isNotificationText() {
        return notificationText;
    }

    public int isNotificationEmail() {
        return notificationEmail;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getNeverEnding() {
        return neverEnding;
    }

    public void setNeverEnding(int neverEnding) {
        this.neverEnding = neverEnding;
    }

    public void setSchedule(int schedule) {
        this.schedule = schedule;
    }

    public void setNotificationAlarm(int notificationAlarm) {
        this.notificationAlarm = notificationAlarm;
    }

    public void setNotificationText(int notificationText) {
        this.notificationText = notificationText;
    }

    public void setNotificationEmail(int notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    public void addMedication() {

        //MyApplication.p("Add Medication");
        new ScheduledMedication().execute(this);

    }

    public boolean medicationExists() {

        return Database.medicationRecordExists(this);
    }

    public void deleteMedication() {
        new DeleteMedication().execute(this);
    }

    public static class MedicationToHomeActivity {
        ;
    }

    public static class ReturnToMedicationActivity {
        ;
    }

    protected static class ScheduleCustomMedication extends AsyncTask<MedicationData[], Void, Long> {

        @Override
        protected Long doInBackground(MedicationData[]... data) {

            MedicationData md[] = data[0];
            long success = -1;
            Log.i("Length is", data.length + "");
            if (data.length == 1) {
                for (int i = 0; i < data[0].length; i++) {
                    success = Database.addMedication(md[i]);
                }

            }
            return success;
        }

        @Override
        protected void onPostExecute(Long id) {


            if (id != -1)
                EventBus.getDefault().post(new MedicationToHomeActivity());
            else
                EventBus.getDefault().post(new ReturnToMedicationActivity());

        }

    }

    protected class DeleteMedication extends AsyncTask<MedicationData, Void, Cursor> {

        @Override
        protected Cursor doInBackground(MedicationData... medicationDatas) {

            Database.deleteMedicationFromDatabase(medicationDatas[0]);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
            String currentDate = sdf.format(new Date());
            return Database.getUpcomingDosagesWithId(MyApplication.getUser().getUSER_NAME(), currentDate);

        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            EventBus.getDefault().post(cursor);
        }
    }

    protected class ScheduledMedication extends AsyncTask<MedicationData, Void, Long> {

        @Override
        protected void onPreExecute() {

            EventBus.getDefault().post(new Constants.StartProgress());
        }

        @Override
        protected Long doInBackground(MedicationData... data) {

            long success = -1;
            Log.i("Length is", data.length + "");
            if (data.length == 1) {

                MedicationData md = new MedicationData(
                        data[0].getMedicationName(),
                        data[0].getUsername(),
                        data[0].getMg(),
                        data[0].isSchedule(),
                        data[0].getNoOfPills(),
                        data[0].getStartDate(),
                        data[0].getStartTime(),
                        data[0].getEndTime(),
                        data[0].getNeverEnding(),
                        data[0].getFrequency(),
                        data[0].isNotificationAlarm(),
                        data[0].isNotificationText(),
                        data[0].isNotificationEmail()

                );
                success = Database.addMedication(md);

                switch (md.getFrequency()) {

                    case 2:
                        addHours(md, 2);
                        break;
                    case 3:
                        addHours(md, 3);
                        break;
                    case 4:
                        addHours(md, 4);
                        break;
                    case 5:
                        addHours(md, 5);
                        break;
                    case 6:
                        addHours(md, 6);

                }

            }
            return success;
        }

        @Override
        protected void onPostExecute(Long id) {

            EventBus.getDefault().post(new Constants.StopProgress());

            if (id != -1)
                EventBus.getDefault().post(new MedicationToHomeActivity());
            else
                EventBus.getDefault().post(new ReturnToMedicationActivity());

        }

        private void addHours(MedicationData md, int frequency) {

            DateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.US);
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy", Locale.US);
            Calendar c2 = Calendar.getInstance();


            MedicationData md1 = new MedicationData(md);

            for (int i = 0; i < frequency - 1; i++) {

                try {
                    c2.setTime(new SimpleDateFormat("MM/dd/yy hh:mm a", Locale.US).parse(md1.getStartDate() + " " + md1.getStartTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c2.add(Calendar.HOUR_OF_DAY, 24 / frequency);
                md1.setStartDate(sdf.format(c2.getTime()));
                md1.setStartTime(dateFormat.format(c2.getTime()));
                Database.addMedication(md1);
            }


        }

    }
}
