package com.example.ektagomitbharani.marble;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;


public class Account extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        final EditText fname = (EditText) findViewById(R.id.fname);
        final EditText lname = (EditText) findViewById(R.id.lname);
        final EditText uname = (EditText) findViewById(R.id.uname);
        final EditText pwd = (EditText) findViewById(R.id.pwd);
        final EditText email = (EditText) findViewById(R.id.email);
        final EditText phone = (EditText) findViewById(R.id.phone);

        findViewById(R.id.finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String first_name = fname.getText().toString();
                String last_name = lname.getText().toString();
                String username = uname.getText().toString();
                String password = pwd.getText().toString();
                String user_email = email.getText().toString();
                String user_phone = phone.getText().toString();

                if (first_name.isEmpty() || last_name.isEmpty() || username.isEmpty() ||
                        password.isEmpty() || user_email.isEmpty() || user_phone.isEmpty()) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(Account.this);
                    dialog.setMessage("Please fill all the details and proceed")
                            .setTitle("Alert !")
                            .setPositiveButton(android.R.string.ok, null);

                    AlertDialog dialogBox = dialog.create();
                    dialogBox.show();

                } else {
                    new DatabaseAddUser().execute(new User(username, password, first_name, last_name, user_email, user_phone));
                }

            }
        });
    }

    protected class DatabaseAddUser extends AsyncTask<User, Void, User> {

        @Override
        protected User doInBackground(User... users) {
            Log.i("Ekta Bharani", users[0].getFIRST_NAME() + "");
            if (users.length == 1)
                Database.addUser(users[0]);

            return users[0];
        }

        @Override
        protected void onPostExecute(User user) {

           // Toast.makeText(Account.this, " Count " + Database.getCount(), Toast.LENGTH_SHORT).show();
            MyApplication.setUser(user);
            startActivity(new Intent(Account.this,Home.class));
            finish();
        }
    }

}
